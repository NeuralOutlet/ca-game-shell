
;; Global game variables
(defvar *board-width* 10)
(defvar *board-height* 20)
(defvar *default-key* #\x)
(defvar *end-game* nil)
(defvar *score* 0)

;; The cell board
(defun generate-board (width height)
        (make-array `(,height ,width) :element-type '(complex) :initial-element #c(0 0)))
(defvar *board* (generate-board *board-width* *board-height*))

;; ===--- API for the GUI/CLI layers ---=== ;;

(defun set-cell-state (board x y val)
	(setf (aref board y x) val))

(defun get-cell-real-state (x y)
	(realpart (aref *board* y x)))

(defun get-cell-imag-state (x y)
	(imagpart (aref *board* y x)))

(defun get-cell-index (x y)
	(cell-index (aref *board* y x)))

(defun get-cell-state (board x y)
	(aref board y x))

(defun get-game-default-key ()
	*default-key*)

(defun get-game-score ()
	*score*)

(defun get-end-game ()
	*end-game*)

;; ===--- the core functions of the games ---=== ;;

;; Apply CA rules for this universe
(defun apply-rules (board input)
        (let ((next-board (generate-board *board-width* *board-height*)))
                (loop for x from 0 below *board-width* do
                        (loop for y from 0 below *board-height*
                                do (set-cell-state next-board x y
                                        (next-state (get-neighbourhood board x y) input))))
                (universe-event board next-board input)))

;; A version of game-loop without the loop
(defun game-single-cycle (board input)
	(apply-rules board input))

;; ===--- re-define these functions ---=== ;;

;; takes a board and return cell & neighbouring cells
(defun get-neighbourhood (board x y) (list))

;; takes cell list and applies the game rules to it
(defun next-state (cell-list user-input) (complex 0 0))

;; takes a complex value - returns an int
(defun cell-index (cell) 0)

;; this is for global events that require non-CA actions
;; An example is adding a new cell randomly to the board
(defun universe-event (board next-board input) board)

;; Start again
(defun reset-game ()
    (setf *board* (generate-board *board-width* *board-height*))
    (setf *score* 0))

;; close application
(defun end-game () (setq *end-game* t))


;; ========================================================================= ;;

(defun reset-game ()
	(setf *board* (generate-block (generate-board *board-width* *board-height*))))

(defvar *block-colour* 3) ; active cells are all coloured the same
(defun cell-index (cell)
	(cond
		((eq (realpart cell) 15) 1) ;dead cell
		((> (realpart cell) 0) *block-colour*)
		(t 0)))

(setf *default-key* #\s)
;; ========================================================================= ;;

;; Get setup
(setf *random-state* (make-random-state t))

;; Tetromino Blocks
(defvar *tetrominoes* (list
	(make-array 4 :initial-contents #((-1 . 0) ( 0 . 0) (-1 . 1) (0 . 1)))
	(make-array 4 :initial-contents #((-1 . 1) (-1 . 0) ( 0 . 0) (1 . 0)))
	(make-array 4 :initial-contents #((-1 . 0) ( 0 . 0) ( 1 . 0) (0 . 1)))
	(make-array 4 :initial-contents #((-2 . 0) (-1 . 0) (0 . 0) (1 . 0)))
	(make-array 4 :initial-contents #((-1 . 0) (0 . 0) (1 . 0) (1 . 1)))
	(make-array 4 :initial-contents #((-1 . 1) (0 . 1) (0 . 0) (1 . 0)))
	(make-array 4 :initial-contents #((-1 . 0) (0 . 0) (0 . 1) (1 . 1)))))


;; just a test
(defun cycle (input)
	(setf *board* (game-single-cycle *board* input))
	*board*)


;; A safe-check function for bounded positions on the board
(defun peak-board (board x y)
	(cond
		((or (< x 0) (>= x *board-width* )) nil)
		((or (< y 0) (>= y *board-height*)) nil)
		(t (realpart (aref board y x)))))

;; For dealing with freezing the whole board
(defvar *freeze-all-active* t)
(defun freeze (board)
	(let ((next-board (generate-board *board-width* *board-height*)))
		(loop for x from 0 below *board-width* do
			(loop for y from 0 below *board-height*
				do (setf (aref next-board y x)
				         (if (> (realpart (aref board y x)) 0)
				         	15 0))))
	next-board))

;;; A Moore neighbourhood is defined as 9 cells indexed in a list as:
;
;  6 7 8
;  5 0 1
;  4 3 2
;
; The indexing is a clockwise outward spiral
(defun get-neighbourhood (board x y)
	(list
		(peak-board board x y)             ; 0
		(peak-board board (1+ x) y)        ; 1
		(peak-board board (1+ x) (1+ y))   ; 2
		(peak-board board x (1+ y))        ; 3
		(peak-board board (1- x) (1+ y))   ; 4
		(peak-board board (1- x) y)        ; 5
		(peak-board board (1- x) (1- y))   ; 6
		(peak-board board x (1- y))        ; 7
		(peak-board board (1+ x) (1- y)))) ; 8


(defun next-state (neighbourhood input)
	(let (state below-state above-state test)
		(multiple-value-setq (state below-state above-state)
		  (cond
			((member input '("n" "s" "e" "w") :test #'equal)
				(values     ;; ROTATE IS  ^  CURRENTLY HERE
					(nth 0 neighbourhood)
					(nth 3 neighbourhood)
					(nth 7 neighbourhood)))
			((equal input "a")
				(values
					(nth 0 neighbourhood)
					(nth 5 neighbourhood)
					(nth 1 neighbourhood)))
			((equal input "d")
				(values
					(nth 0 neighbourhood)
					(nth 1 neighbourhood)
					(nth 5 neighbourhood)))
			(t (progn (format t "ahhhh~%") (values nil nil nil)))))
		(format t "    [~a, ~a, ~a]~%" state below-state above-state)
		(setq test (cond
			((eq state 15) 15)
			((and (eq state 0) (eq above-state 15)) 0)
			((eq state 0) (if (numberp above-state) above-state 0))
			((and (> state 0) (< state 15) (or (null below-state) (eq below-state 15)))
				(progn (setf *freeze-all-active* t) 15))
			(t (if (numberp above-state) above-state 0))))
		;(format t "end: ~a~%" test)
		test))


;; Apply CA rules including universal effects
(defun universe-event (board next-board input)
	(let ((frozen *freeze-all-active*))
		(setf *freeze-all-active* nil)
		(if frozen
			(if (equal input "s") 
				(generate-block (clear-lines (freeze board))) ; freeze board and generate new block
				board)                          ; stop certain movements (left & right)
			(if (equal "e" input)
				(apply-rules next-board input)    ; recursive call to drop block
				next-board))))                    ; normal one space drop


;; Check if a ihorizontal line is full at row y
(defun is-line-full (board y)
	(let ((results
		(loop for x from 0 below *board-width*
			when (eq (realpart (aref board y x)) 15)
			collect t)))
		(if (eq (length results) 10) t)))

;; Remove singular lines and drop above lines down
(defun clear-lines (board)
	(let ((next-board (generate-board *board-width* *board-height*))
	      (linecount (1- *board-height*)))
		(loop for y from linecount above -1
			when (not (is-line-full board y))
			do (progn
				(loop for x from 0 below *board-width*
					do (setf (aref next-board linecount x) (aref board y x)))
				(if (> linecount -1)
					(setf linecount (1- linecount)))))
		next-board))


;; Randomly place a random type of tetromino on the board
(defun generate-block (board)
	(let* ((index (random 7))
	       (block (nth index *tetrominoes*))
	       (y (1+ (random 8)))
	       (x 0))

		(setf *block-colour* (1+ (1+ (random 5))))
		;; safely generate block
		(setf *freeze-all-active* nil)
		(if (eq index 3)
			(if (< y 5)
				(setf y (1+ y))))

		(format t "genb;ock~%")
		(loop for i from 0 below 4
			for b = (car (aref block i))
			for a = (cdr (aref block i))
			do (setf (aref board (+ x a) (+ y b)) (1+ index)))
		board))

;; Check if the board state is a fail/win state
(defun end-game (board) nil)

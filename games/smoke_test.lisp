
(defun reset-game ()
	(generate-board *board-width* *board-height*))

;; Some states may want to be displayed as the same
;; colour so this is an easy way to regulate that
(defun cell-index (cell)
	(cond
		((eq (realpart cell) 0) 0)
		((eq (realpart cell) 1) 1)
		((eq (realpart cell) 2) 2)
		((eq (realpart cell) 3) 3)
		((eq (realpart cell) 4) 4)
		((eq (realpart cell) 5) 5)
		(t 6)))

(defun universe-event (board next-board input)
	next-board)

;;; REAL:      {0=null, 1=head, 2=body, 3=food}
;;; IMAGINARY: {0=null, 1=left, 2=up, 3=right, 4=down}

;; the generic next state function (should have the largest function body)
(defun next-state (cell-list state)
	(let ((state (car cell-list)))
		(cond
			((eq state 0) (complex 1 0))
			((eq state 1) (complex 2 0))
			((eq state 2) (complex 3 0))
			((eq state 3) (complex 4 0))
			((eq state 4) (complex 5 0))
			((eq state 4) (complex 6 0))
			(t (progn
			     (end-game) #c(0 0))))))

;;; A Von Neumann neighbourhood is defined as 5 cells indexed in a list as:
;
;  - 4 -
;  3 0 1
;  - 2 -
;
; The indexing is teh inverse of the states to easily define towards middle state
(defun get-neighbourhood (board x y)
	(list
		(peak-board board x y)
		(peak-board board (1+ x) y)
		(peak-board board x (1+ y))
		(peak-board board (1- x) y)
		(peak-board board x (1- y))))

;; a cycle of the game using the global *board*
(defun cycle (input)
	(setq *board* (game-single-cycle *board* 2))
	*board*)

;; A safe-check function for bounded positions on the board
(defun peak-board (board x y)
	(cond
		((< x 0)   (get-cell-state board (1- *board-width*) y))
		((>= x *board-width*) (get-cell-state board 0 y))
		((< y 0)   (get-cell-state board x (1- *board-height*)))
		((>= y *board-height*) (get-cell-state board x 0))
		(t (get-cell-state board x y))))

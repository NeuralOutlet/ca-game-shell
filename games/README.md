

# Game of Snake #

This is a super basic snake game, no treats just a snake moving around a rectangle and eating bits of food growing larger. The complex states are made up of snake states and ghost states. The user inputs are one of four directional states and they create a sort of 'ghost snake' that the snake states use during state change.


# Game of Tetris #

The idea is an extension of [this blog post](https://neuraloutlet.wordpress.com/2012/04/02/numeral-system-automata/) on modeling number systems as cellular automata. The rules aren't complete yet but the plan is to have states for rotation of each tetris block, states for moving, and states for dropping seperated in an 'aesthetic' way by having cell states be complex values. In this case user input can be on the imaginary plane {0i: no input, 1i: left, 2i: down, 3i: right, 4i: rotate}. The cellular automata uses a 3x3 Moorse neighbourhood and I want the four-cell line piece to have a mid-stage of the block piece (vertical-line -> block -> horizontal line).



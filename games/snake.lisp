
(defvar *current-ghost* 3)
(defvar *reset-food* t)
(defvar *foodx* 8)
(defvar *foody* 8)

;; ========================================================================= ;;

(setq *default-key* #\e)

(defun reset-game ()
	(setf *board* (generate-board *board-width* *board-height*))
	(set-cell-state *board* 2 5 #c(2 3))
	(set-cell-state *board* 3 5 #c(2 3))
	(set-cell-state *board* 4 5 #c(2 3))
	(set-cell-state *board* 5 5 #c(1 3))
	(setf *current-ghost* 3)
	(setf *score* 0)
	(generate-food))

;; Some states may want to be displayed as the same
;; colour so this is an easy way to regulate that
(defun cell-index (cell)
	(cond
		((eq (realpart cell) 0) 0)
		((eq (realpart cell) 1) 6)
		((eq (realpart cell) 2) 2)
		((and (eq (realpart cell) 3) (not (eq (imagpart cell) 0))) 4) ; snake
		((eq (realpart cell) 3) 5) ; food
		((eq (realpart cell) -1) 5)
		((eq (realpart cell) -2) 4)
		((eq (realpart cell) -3) 1)
		(t 0)))

(defun universe-event (board next-board input)
	(if *reset-food*
		(progn (generate-food) (setq *reset-food* nil)
			(set-cell-state next-board *foodx* *foody* (complex 3 0))))
	next-board)

;;; REAL:      {0=null, 1=head, 2=body, 3=food}
;;; IMAGINARY: {0=null, 1=left, 2=up, 3=right, 4=down}

;; the generic next state function (should have the largest function body)
(defun next-state (neighbourhood current-ghost)
	(let* ((breakdown-data (breakdown neighbourhood))
	       (state (car breakdown-data))
	       (ghost (nth 1 breakdown-data))
	       (headtowards (nth 2 breakdown-data))
	       (bodytowards (nth 3 breakdown-data))
	       (tailtowards (nth 4 breakdown-data))
	       (foodtowards (nth 5 breakdown-data))
	       (death (nth 6 breakdown-data)))
		(cond
			((eq state -1) (complex -2 0)) ; dead cells
			((eq state -2) (complex -3 0)) ; dead cells
			((eq state -3) (complex -3 0)) ; dead cells
			((and (eq state 2) (> headtowards 0)) (complex -1 0)) ; end game
			((and (not (eq state 0)) (> death 0)) (complex -3 0)) ; dying cell
			((and (eq state 0) (> death 0)) (complex -1 0)) ; dying cell
			((and (eq state 0) (eq headtowards 1)) (complex 1 *current-ghost*))
			((and (eq state 0) (eq bodytowards 1)) (complex 1 *current-ghost*))
			((eq state 1) (complex 2 ghost))
			((and (eq state 2) (eq bodytowards 1)) (complex 2 ghost))
			((and (eq state 2) (eq foodtowards 1)) (complex 3 ghost))
			((and (eq state 3) (eq tailtowards 1)) (progn (setq *reset-food* t) (complex 2 ghost)))
			((and (eq state 3) (eq bodytowards 1)) (complex 3 ghost))
			((and (eq state 3) (eq headtowards 1)) (complex 3 *current-ghost*))
			((eq state 3) #c(3 0))
			((eq state 2) #c(0 0))
			(t #c(0 0)))))

;;; A Von Neumann neighbourhood is defined as 5 cells indexed in a list as:
;
;  - 4 -
;  3 0 1
;  - 2 -
;
; The indexing is the inverse of the states to easily define towards middle state
(defun get-neighbourhood (board x y)
	(list
		(peak-board board x y)
		(peak-board board (1+ x) y)
		(peak-board board x (1+ y))
		(peak-board board (1- x) y)
		(peak-board board x (1- y))))

;; a cycle of the game using the global *board*
(defun cycle (input)
	(setq *board* (game-single-cycle *board* (input-rule input)))
	(setq *current-ghost* (input-rule input))
	*board*)

;; ========================================================================= ;;


;; Apply the correct rule based on state
(defun input-rule (input)
	(cond
		((equal input "e") *current-ghost*)
		((equal input "a") 1)
		((equal input "w") 2)
		((equal input "d") 3)
		((equal input "s") 4)
		(t *current-ghost*)))

;; A safe-check function for bounded positions on the board
(defun peak-board (board x y)
	(cond
		((< x 0)   (get-cell-state board (1- *board-width*) y))
		((>= x *board-width*) (get-cell-state board 0 y))
		((< y 0)   (get-cell-state board x (1- *board-height*)))
		((>= y *board-height*) (get-cell-state board x 0))
		(t (get-cell-state board x y))))

;; returns the state, ghost state, and any possible state interactions
(defun breakdown (neighbourhood)
	(let* ((rstate (realpart (car neighbourhood)))
	       (istate (imagpart (car neighbourhood)))
	       (headc 0) (bodyc 0) (tailc 0) (foodc 0) (deadc 0))

		; now count the interactions with centre state
		(loop for state in (cdr neighbourhood)
		      for i from 1 to 4
			do (cond
				((and (eq (realpart state) 1) (eq i (imagpart state)))
				  (setq headc (1+ headc)))
				((and (eq (realpart state) 3) (eq i (imagpart state)))
				  (setq bodyc (1+ headc)))
				((and (eq (realpart state) 2) (eq i (imagpart state)))
				  (setq bodyc (1+ bodyc)))
				((and (eq (realpart state) 3) (eq i (imagpart state)))
				  (setq foodc (1+ foodc)))
				((and (eq i istate) (eq (realpart state) 0) (eq rstate 3))
				  (setq tailc (1+ tailc)))
				((eq (realpart state) -1) (setq deadc (1+ deadc)))
				(t nil)))

		(list rstate istate headc bodyc tailc foodc deadc)))

;; recursively searches for a free area on the board to put food
(defun generate-food ()
	(let ((fx (random *board-width*))
	      (fy (random *board-height*)))
		(if (eq (peak-board *board* fx fy) 0)
			(progn
				(setf *foodx* fx)
				(setf *foody* fy)
				(setf *score* (+ *score* 50)))
				(generate-food))))

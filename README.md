# The CA Game Shell #

For the multiple user interfaces listed furter below the game shell is the standardised way of interacting with the game rules. It is written in vanilla Common Lisp so it isn't restricted to a specific flvour. I wanted to write the games using a strictly functional paradigm, but alas [I am not a clever man](http://i.imgur.com/a4CVG.jpg). Overall it is a few function that need redefining by the games and some accessor functions. The ones needed to write a game file are:

   
    ;; takes a board and return cell & neighbouring cells
    (defun get-neighbourhood (board x y) (list))
    
    ;; takes cell list and applies the game rules to it
    (defun next-state (cell-list user-input) cell-state)
    
    ;; takes a complex value - returns an int
    (defun cell-index (cell) index)
    
    ;; this is for global events that require non-CA actions
    ;; An example is adding a new cell randomly to the board
    (defun universe-event (board next-board input) board)
    
    ;; Start again
    (defun reset-game ()
    	(setf *board* (generate-board *board-width* *board-height*))
    	(setf *score* 0))

The game shell loads in a rule set which overwrites the above functions. All cellular automata is expected to be of complex value states. I chose to do it this way to distinguish user input from normal state change, all user inputs are registered as imaginary state changes. The game itself is written in Common Lisp and then there are various user interfaces for it in C++/Java/Lisp because ¯\\\_(ツ)\_/¯ why not.



# Multiple User Interfaces for Lisp Games #

There are three GUIs and one CLI to choose from for playing the games:

* **Graphical User Interfaces**
    * ABCL: Using Swing for GUI (Java libraries)
    * SBCL: Using LTK for GUI (Common Lisp)
    * ECL:  Using Qt for GUI (C++)
    
* **Command Line Interface**
    * SBCL: Using UFFI and sb-threads (Common Lisp\*)

 \* UFFI is used with a compiled termios.h function for unbuffered keyboard input so there's a little C.



# Building & Running #

Build instructions are in each GUI folder. The base level ./build.sh will attempt to build all and list requirements.

Currently the Qt interface has a file selection button for picking which game to play, but for the other you run them from the commandline with the path to the game as the argument, for example to play snake with gui\_lisp\_ltk\_sbcl you would type:

    $ ./cell-games.lisp ../../games/snake.lisp


#ifndef INTERFACE_LAYER_H
#define INTERFACE_LAYER_H

extern "C" {

void init_shell(int argc, char *argv[]);
void load_game(const char *gamePath);
int c_get_cell_rstate(int x, int y);
int c_get_cell_istate(int x, int y);
int c_get_cell_index(int x, int y);
int c_get_game_score();
char c_get_game_default_key();
bool cycle_game_loop(char _input);
bool c_get_end_game();
void reset_game();

} // end extern C

#endif // INTERFACE_LAYER_H

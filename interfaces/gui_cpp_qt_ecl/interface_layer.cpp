#include "interface_layer.h"

#include <stdlib.h>
#include <ecl/ecl.h>

extern "C" {

/// The wrapper functions
///
/// load_game and init_shell

void init_shell(int argc, char *argv[])
{
	// Initialise ECL interpreter and load game
	cl_boot(argc, argv);

	// Load the shell all game files are based off of
	cl_safe_eval(c_string_to_object("(load \"../../../ca_shell.lisp\")"), Cnil, Cnil);

	if (argc > 1)
	{
		char *val = argv[1];

		FILE *file;
		if (file = fopen(val, "r"))
		{
			fclose(file);
			char str[256];
			sprintf(str, "(load \"%s\")", val);
			cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
		}
	}
}

void load_game(const char *gamePath)
{
	char str[256];
	sprintf(str, "(load \"%s\")", gamePath);
	cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
}

int c_get_cell_rstate(int x, int y)
{
	char str[256];
	sprintf(str, "(get-cell-real-state %d %d)", x, y);
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return (int)ecl_to_float(result);
}


int c_get_cell_istate(int x, int y)
{
	char str[256];
	sprintf(str, "(get-cell-imag-state %d %d)", x, y);
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return (int)ecl_to_float(result);
}

int c_get_cell_index(int x, int y)
{
	char str[256];
	sprintf(str, "(get-cell-index %d %d)", x, y);
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return (int)ecl_to_float(result);
}

char c_get_game_default_key()
{
	char str[256];
	sprintf(str, "(get-game-default-key)");
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return ecl_to_char(result);
}

bool cycle_game_loop(char _input)
{
	char lspfnc[256];
	sprintf(lspfnc, "(cycle \"%c\")", _input);
	cl_safe_eval(c_string_to_object(lspfnc), Cnil, Cnil);
	return true;
}

void reset_game()
{
	char lspfnc[256];
	sprintf(lspfnc, "(reset-game)");
	cl_safe_eval(c_string_to_object(lspfnc), Cnil, Cnil);
}

int c_get_game_score()
{
	char str[256];
	sprintf(str, "(get-game-score)");
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return (int)ecl_to_float(result);
}

bool c_get_end_game()
{
	char str[256];
	sprintf(str, "(get-end-game)");
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return ecl_to_bool(result);
}

} // end extern C


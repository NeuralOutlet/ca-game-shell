#include "cellularshell.h"
#include <QApplication>

#include "../../interface_layer.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    init_shell(argc, argv);

    BaseTetris w;
    w.show();

    return a.exec();
}

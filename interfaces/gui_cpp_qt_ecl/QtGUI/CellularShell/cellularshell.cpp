#include "cellularshell.h"
#include "ui_cellularshell.h"

#include <QGridLayout>
#include <QSpacerItem>
#include <QPushButton>
#include <QFileDialog>
#include <QGroupBox>
#include <QTimer>

#include "../../interface_layer.h"

// Add a skin to the cell
QPixmap skin_cell(QString _img);

BaseTetris::BaseTetris(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BaseTetris), m_input('e')
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/cells/TetrisAutomataIcon.png"));
    setWindowTitle("Cellular Automata Games");

    QPixmap emptycell(25, 25); emptycell.fill(Qt::transparent);
    m_cellImage[0] = emptycell;
    m_cellImage[1] = skin_cell(":/cells/Grey Cell.png");
    m_cellImage[2] = skin_cell(":/cells/Green Cell.png");
    m_cellImage[3] = skin_cell(":/cells/Purple Cell.png");
    m_cellImage[4] = skin_cell(":/cells/Yellow Cell.png");
    m_cellImage[5] = skin_cell(":/cells/Red Cell.png");
    m_cellImage[6] = skin_cell(":/cells/Blue Cell.png");

    QGridLayout *lay = new QGridLayout;
    for (int row = 0; row < 20; ++row)
        for (int col = 0; col < 10; ++col)
            lay->addWidget(&m_board[row][col], row, col, 1, 1, Qt::AlignCenter);

    p_score = new QLabel("<b>Score: 0</b>");
    p_score->setStyleSheet("background-color: rgb(140,170,140);");
    p_score->setTextFormat(Qt::RichText);
    QPushButton *resetButt = new QPushButton("Reset");
    QPushButton *loadButt = new QPushButton("Load Game");
    QString buttStyle = "QPushButton {"
        "    border: 2px solid rgb(50,70,50);"
        "    border-radius: 4px;"
        "    background-color: rgb(120,140,140);"
        "    min-width: 60px;"
        "    min-height: 20px;"
        "}"

        "QPushButton:pressed {"
        "    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,"
        "                                      stop: 0 rgb(120,140,140), stop: 1 rgb(50,70,50));"
        "    border-style: inset;"
        "}";
    resetButt->setStyleSheet(buttStyle);
    loadButt->setStyleSheet(buttStyle);


    QGroupBox *box = new QGroupBox;
    QGridLayout *boxLay = new QGridLayout;
    boxLay->addWidget(p_score, 0, 0, 1, 2, Qt::AlignLeft);
    boxLay->addItem(new QSpacerItem(400, 10, QSizePolicy::Expanding), 0, 2, 1, 6, Qt::AlignCenter);
    boxLay->addWidget(loadButt, 0, 6, 1, 2, Qt::AlignRight);
    boxLay->addWidget(resetButt, 0, 8, 1, 2, Qt::AlignRight);
    box->setLayout(boxLay);
    box->setStyleSheet("QGroupBox { border: 1px solid; border-color: rgb(40,70,40); background-color: rgb(140,170,140); }");

    lay->addWidget(box, 20, 0, 2, 10, Qt::AlignCenter);

    setLayout(lay);
    resize(326, 636);
    setStyleSheet("QWidget { background-color: rgb(120, 140, 140); }");

    p_timer = new QTimer;
    p_timer->setInterval(250);

    /// --------------------

    connect(p_timer, SIGNAL(timeout()), this, SLOT(moveTimeout()));
    connect(resetButt, SIGNAL(pressed()), this, SLOT(resetBoard()));
    connect(loadButt, SIGNAL(pressed()), this, SLOT(loadGame()));

    // for commandline loaded games
    resetBoard();
}

BaseTetris::~BaseTetris()
{
    delete ui;
}

void BaseTetris::resetBoard()
{
    // Clear the board, set the score to 0 and add random food
    reset_game();
    m_input = 'e';

    // Stop the pushbutton consuming arrow keys
    setFocus();

    p_timer->start();
}

void BaseTetris::setBoard()
{
    if (c_get_end_game())
        close();

    for (int row = 0; row < 20; ++row)
    {
        for (int col = 0; col < 10; ++col)
        {
            int cellTypeID = c_get_cell_index(col, row);
            m_board[row][col].setPixmap(m_cellImage[cellTypeID]);
        }
    }

    int score = c_get_game_score();
    p_score->setText("<b>Score: " + QString::number(score) + "</b>");
}

void BaseTetris::moveTimeout()
{
    cycle_game_loop(m_input);
    setBoard();
}

void BaseTetris::loadGame()
{
    p_timer->stop();

    //Open load dialog
    QString fileName = QFileDialog::getOpenFileName(this, QString("Open Cellular Game File"), "", QString("Lisp Files (*.cl *.lsp *.lisp)"));
    load_game(qPrintable(fileName));
    resetBoard();

    // Set the game's default input
    m_input = c_get_game_default_key();

    p_timer->start();
}

void BaseTetris::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_W:
    case Qt::Key_Up:
        m_input = 'w'; // Rotate
        break;
    case Qt::Key_A:
    case Qt::Key_Left:
        m_input = 'a';
        break;
    case Qt::Key_S:
    case Qt::Key_Down:
        m_input = 's';
        break;
    case Qt::Key_D:
    case Qt::Key_Right:
        m_input = 'd';
        break;
    case Qt::Key_Space:
    case Qt::Key_Enter:
        m_input = 'e'; // Special
        break;
    case Qt::Key_Q:
        close();
        break;
    default:
        qDebug("Unrecognised key :(");
        break;
    }

    // Update the board whatever the case
    setBoard();

    // pass to parent
    QWidget::keyPressEvent(event);
}

QPixmap skin_cell(QString _img)
{
    QImage pix = QImage(_img).scaled(25,25,Qt::KeepAspectRatio, Qt::SmoothTransformation);
    for (int x = 0; x < pix.width(); ++x)
    {
        for (int y = 0; y < pix.height(); ++y)
        {
            QColor c = pix.pixel(x, y);
            qreal h = c.hslHueF();
            qreal s = c.saturationF() * 0.25;
            qreal l = c.lightnessF() * 0.6;
            pix.setPixel(x,y, QColor::fromHslF(h,s,l).rgb());
        }
    }
    return QPixmap::fromImage(pix);
}

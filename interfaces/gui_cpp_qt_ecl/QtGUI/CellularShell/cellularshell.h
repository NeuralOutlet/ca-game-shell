#ifndef BASETETRIS_H
#define BASETETRIS_H

#include <QWidget>
#include <QLabel>
#include <QKeyEvent>

namespace Ui {
class BaseTetris;
}

class BaseTetris : public QWidget
{
    Q_OBJECT

public:
    explicit BaseTetris(QWidget *parent = 0);
    ~BaseTetris();

    int getCellColourID();
    int countEmptyCells();

public slots:
    void setBoard();
    void moveTimeout();
    void resetBoard();
    void loadGame();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::BaseTetris *ui;
    QPixmap m_cellImage[7];
    QLabel m_board[20][10]; // row x column
    int m_colourIndex;
    int m_emptyCount;

    QLabel *p_score;

    // game time thread
    QTimer *p_timer;
    char m_input;
};

#endif // BASETETRIS_H


# C++ version (Using ECL) #

Embedded Common Lisp (ECL) is lovely and magical and compatible with C.

### Compiling and running from here ###

Requirements: MinGW/GCC, ECL, Qt

    This one was a bit fiddly, to get ecl lib to work with Qt I had to change a few instances of the word 'slots' to 'slits' in object.h and then a few further changes were required for friendliness with Windows (config.h required 'signed' added to the def of int8_t).
    Then just compile with Qt and make sure all the relative paths are correct.


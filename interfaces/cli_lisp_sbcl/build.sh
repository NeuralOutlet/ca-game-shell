#!/bin/sh

export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH
gcc -c -fPIC direct_getch.c -o getch.o
gcc -shared getch.o -o libgetch.so

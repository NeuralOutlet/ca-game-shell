# Command Line Interface (via SBCL)

Steel Bank Common Lisp is a lovely and magical implementation which would feel left out if I didn't use it.

### Running from here ###

Requirements: SBCL

    $ ./num-blocks.lisp <path-to-game>

### Better Keyboard Interaction ###

You don't have to press enter after each command input. A very simple implementation of SBCL's Foreign Function Interface (FFI) lets us use a custom getch() function. To set this up follow the below commands then run num-blocks again.

    $ export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH
    $ gcc -c -fPIC direct_getch.c -o getch.o
    $ gcc -shared getch.o -o libgetch.so

Nice.

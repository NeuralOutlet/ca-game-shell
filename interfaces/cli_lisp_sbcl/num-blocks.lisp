#!/usr/bin/sbcl --script

;; Load the game
(load "../../ca_shell.lisp")

(let ((gamefile (nth 1 sb-ext:*posix-argv*)))
	(if (or (null gamefile) (not (load gamefile)))
		(progn
			(format t "gamefile not found, run with path to lisp game:~%~%    ./num-blocks.lisp ../snake.lisp~%~%")
			(sb-ext:quit))))

;; There are two ways to get the input
;; So here we forward declare one of them
(defvar input #'read-char)
(defvar notice "")

;; If the getch lib is present define ffi
(defun init-controls ()
	(declaim (inline getch))
	(define-alien-routine "getch" int)
	(defun cgetch ()
		(code-char (with-alien ()
			(alien-funcall (extern-alien
				"getch" (function int))))))
	; The other way of getting input
	(setf input #'cgetch))

(defun safe-controls (se)
	(declare (ignore se))
	(setf notice
		" (press ENTER after input)"))

;; Safely load getch lib, if not there ignore
(handler-case
		(load-shared-object "./libgetch.so")
	(simple-error (se) (safe-controls se))
	(:no-error (se) (init-controls)))


;; Overwrite the get-user-input function
;; with an SBCL specific timed input:
(defun get-user-input ()
	(let* ((result nil)
	       (user-thread (sb-thread:make-thread #'(lambda ()
	           (setq result (string (funcall input)))) :name "userinp"))
	       (time-thread (sb-thread:make-thread #'(lambda ()
	           (sleep 1) (setq result (string *default-key*))) :name "userlim")))

	(loop while (and (sb-thread:thread-alive-p user-thread)
                 (sb-thread:thread-alive-p time-thread)))

	(if (sb-thread:thread-alive-p user-thread)
		(sb-thread:terminate-thread user-thread))

	(if (equal result "q")
		(sb-ext:exit))
	result))

;; An ANSI coloured output function:
(defun colour-string (str col)
	(let ((out-col (if (eq col 45) 30 col)))
		(cond 
			((eq col 31) (format nil "~C[~am~a~C[0m" #\Esc 90 str #\Esc))
			((> col 30) (format nil "~C[~am~a~C[0m" #\Esc out-col str #\Esc))
			(t " "))))

;; Overwrite the set-board function
;; to be a little bit more fancy:
(defun set-board (board)
	(let ((sbcl-board (loop for y from 0 below *board-height*
		      collect (loop for x from 0 below *board-width*
		      collect (colour-string "*"
		                  (+ 30 (get-cell-index x y)))))))

	; Print it all lovely like
	(format t "~C[2J" #\Esc) ; Clear screen
	(format t "~C┏━━━━━━━━━━━━━━━━━━━━┓~%" #\Tab)
	(format t "~C┃     NUM BLOCKS     ┃~%" #\Tab)
	(format t "~C┣━━━━━━━━━━━━━━━━━━━━┫~%" #\Tab)
	(loop for i from 0 below *board-height*
		do (format t "~C┃~{~a ~}┃~%" #\Tab (nth i sbcl-board)))
	(format t "~C┗━━━━━━━━━━━━━━━━━━━━┛~%" #\Tab)
	(format t "~%~a~%Spin:  W
Left:  A
Right: D
Down:  S   (Drop: E)

Quit:  Q~%" notice)))

;; Start the game
(reset-game)
(defun main ()
	(let ((input (get-user-input)))
		(cycle input)
		(set-board *board*)
		(if (not (equal input "q"))
			(main))))
(main)

#!/usr/bin/sbcl --script

;; Load LTK
(load "ltk")
(use-package :ltk)

;; Load the game
(load "../../ca_shell.lisp")
(let ((gamefile (nth 1 sb-ext:*posix-argv*)))
	(if (or (null gamefile) (not (load gamefile)))
		(progn
			(format t "gamefile not found, run with path to lisp game:~%~%    ./cell-games.lisp ../snake.lisp~%~%")
			(sb-ext:quit))))

(defvar *ltk-board* nil) ; A board containing the IDs of each rectangle
(defvar *user-input* *default-key*) ; user input passed in by game loop

;; create and display the interface
(defun mainwidget ()
	(reset-game)
	(with-ltk ()
		(let* ((canv (make-instance 'canvas))
			   (text (create-text canv 100 100 "Count down: "))
			   (rect (create-rectangle canv 0 0 50 50))
			   (butt (make-instance 'button
			                       :master nil
			                       :text "reset"
			                       :command (lambda () (reset-game) (set-board canv)))))
			(focus canv)
			(bind canv "<KeyPress>" (lambda (event)
			                      (declare (ignore event))
			                      (setq *user-input* (event-char event))))
			
			(configure canv :width 100)
			(configure canv :height 250)
			(define-board canv)
			(pack canv); :expand 1 :fill :both)
			(pack butt)

			(update-game canv *user-input*)
			(mainloop))))

;; the basic game loop:  cycle the game,
;    focus the canvas (for catching keypresses)
;    re-draw the board, repeat
(defun update-game (canvas input)
	(cycle (string-downcase (string input)))
	(setq *user-input* *default-key*)
	(focus canvas)
	(set-board canvas)
	(after 125 (lambda () (update-game canvas *user-input*))))

(defun define-board (canvas)
	(let ((cells (loop for row from 0 to 9 collect
	          (loop for col from 0 to 19 collect
	            (create-rectangle canvas (* row 10) (* col 10)
	                (+ (* row 10) 10) (+ (* col 10) 10))))))
		(setf *ltk-board* cells)))

(defun set-board (canvas)
	(loop for row from 0 to 9 do
		(loop for col from 0 to 19 do
			(itemconfigure
			    canvas 
			    (nth col (nth row *ltk-board*))
			    :fill (get-colour (get-cell-index row col))))))

(defun get-colour (index)
	(nth index (list "#ffffff" "#373737" "#008f20" "#ff0010" "#823700" "#301090" "#010101")))

(mainwidget)

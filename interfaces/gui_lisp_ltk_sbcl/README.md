
# Lisp version (Using LTK) #

Steel Bank Common Lisp (SBCL) using the LTK package.

### Compiling and running from here ###

Requirements: LTK

    download and compile LTK
    put ltk.lisp in this folder

Then run:

    $ ./cell-games <path-to-game>

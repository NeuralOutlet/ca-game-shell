
import org.armedbear.lisp.*;

public class InterfaceLayer
{
	public static void main(String[] argv)
	{
		try
		{
			long startTime = System.nanoTime();

			Interpreter interpreter = Interpreter.createInstance();
			interpreter.eval("(load \"interface-layer.lisp\")");
			org.armedbear.lisp.Package defaultPackage = Packages.findPackage("CL-USER");

			Symbol getCellSym = defaultPackage.findAccessibleSymbol("J-GET-CELL-STATE");
			Function getCellFunc = (Function) getCellSym.getSymbolFunction();
			LispObject testCell = getCellFunc.execute(new JavaObject(4), new JavaObject(4));
			System.out.print("Cell[4,4] = ");
			System.out.println(testCell.intValue());

			Symbol setCellSym = defaultPackage.findAccessibleSymbol("J-SET-CELL-STATE");
			Function setCellFunc = (Function) setCellSym.getSymbolFunction();
			setCellFunc.execute(new JavaObject(4), new JavaObject(4), new JavaObject(1));
			testCell = getCellFunc.execute(new JavaObject(4), new JavaObject(4));
			System.out.print("Cell[4,4] = ");
			System.out.println(testCell.intValue());

			long endTime = System.nanoTime();
			long duration = (endTime - startTime);
			System.out.print("Duration: ");
			System.out.println(duration);
		}
		catch (Throwable t)
		{
			System.out.println("exception!");
		}
	}
}

import java.awt.event.*;;
import javax.swing.*;
import java.util.*;
import java.awt.*;

public class GUI extends JFrame {

	int spacing = 5;

	public GUI() {
		this.setTitle("Cell Games");
		this.setSize(200, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);

		Board board = new Board();
		this.setContentPane(board);
	}

	public class Board extends JPanel {
		
		int cellX = 5;
		int cellY = 5;

		public Board() {
			InputMap im = getInputMap(WHEN_FOCUSED);
			ActionMap am = getActionMap();

			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "onUp");
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "onDown");
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "onLeft");
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "onRight");

			am.put("onUp", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (cellY > 0) {
						cellY = cellY - 1;
					}
				}
			});

			am.put("onDown", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (cellY < 25) {
						cellY = cellY + 1;
					}
				}
			});

			am.put("onLeft", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (cellX > 0) {
						cellX = cellX - 1;
					}
				}
			});

			am.put("onRight", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (cellX < 10) {
						cellX = cellX + 1;
					}
				}
			});

		}


		public void paint(Graphics g) {
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 25; j++) {

					if (i == cellX && j == cellY)
					{
						g.setColor(Color.red);
						g.fillRect(i*20, j*20, 20, 20);
					}
					else if (((j * 25) + i) % 2 == 0)
					{
						g.setColor(Color.DARK_GRAY);
						g.fillRect(i*20, j*20, 20, 20);
					}
					else
					{
						g.setColor(Color.green);
						g.fillRect(i*20, j*20, 20, 20);
					}

				}
			}
			//g.setColor(Color.red);
			//g.fillRect(0, 0, 10, 500);
		}
	}
}

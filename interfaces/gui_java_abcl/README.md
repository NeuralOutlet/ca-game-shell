
# Java version (Using ABCL) #

Armed Bear Common Lisp (ABCL) is lovely and magical and is written on the JVM.

### Compiling and running from here ###

Requirements: Java, Source/binary of ABCL

    $ export ABCL_ROOT=path/to/abcl.jar
    $ javac -cp $ABCL_ROOT/abcl.jar InterfaceLayer.java
    $ java -cp $ABCL_ROOT/abcl.jar:. InterfaceLayer

or

    $ ./build.sh



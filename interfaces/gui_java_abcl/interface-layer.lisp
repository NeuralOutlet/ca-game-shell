
;; The game
(load "../../ca_shell.lisp")

;; wrapper functions taking the general lisp
;; functions and adding the java-friendly parts

(defun j-get-cell-state (jparam-x jparam-y)
	(let* ((x (jobject-lisp-value jparam-x))
	       (y (jobject-lisp-value jparam-y)))
		(get-cell-real-state x y)))

(defun j-set-cell-state (jparam-x jparam-y jparam-val)
	(let* ((x (jobject-lisp-value jparam-x))
	       (y (jobject-lisp-value jparam-y))
	       (val (jobject-lisp-value jparam-val)))
		(set-cell-state *board* x y (complex val))))


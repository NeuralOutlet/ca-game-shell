
(defcellgame snake "Cell Game Snake"
	:data-prep breakdown
	:triggers '((condition action)...)
	:start start-game
	:end end-game)

(defcellrules name cell-transition-and-laws...)

(deflaw name '(operator (amount complex-value)))

Game of Life:
 1 w/ <2 1s -> 0
 1 w/ >=2 1s & <=3 1s -> 1
 1 w/ >3 1s -> 0
 0 w/ =3 1s -> 1

; defcellrules is a big OR of laws
; deflaw is an AND of laws

(deflaw death1 '((< (2 #c(1 0)))))
(deflaw death2 '((> (3 #c(1 0)))))
(deflaw life '((= (3 #c(1 0)))))

(defcellrules gameofliferules
	#[ 1 0 death1 ]
	#[ 1 0 death2 ]
	#[ 0 1 life ])

(defcellgame gameoflife
	:rules gameofliferules
	:triggers '((condition action)...)
	:start start-game
	:end end-game)

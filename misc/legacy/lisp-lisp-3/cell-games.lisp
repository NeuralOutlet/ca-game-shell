#!./sbclclim --script

(defpackage :mygadget
	(:use :clim :clim-lisp)
(	:export run))

(in-package :mygadget)

;; load the shell
(load "../ca_shell.lisp")


(defgeneric display (frame pane))

(defun run (frame-type)
	(let ((frame (clim:make-application-frame frame-type)))
		(clim:run-frame-top-level frame)))

(defclass abstract-simple-gadget (clim:basic-gadget) ())
(defclass generic-simple-gadget (abstract-simple-gadget)
	((color :initform clim:+darkred+ :accessor gadget-color)))

(clim:define-application-frame gadget-frame ()
	()
	(:panes
		(pane (clim:make-pane 'generic-simple-gadget))
		(butt (make-pane 'push-button
		                :height 1000 :width 60
		                :label "Show"))
		(int :interactor :height 100 :width 200))
		(:layouts
			(default (vertically () pane butt))
			(editmode (vertically () pane butt int))))

(define-gadget-frame-command (com-load :name "test" :menu nil)
	((img-pathname 'pathname
	               :default ""
	               :insert-default t))
	(load img-pathname))

(define-gadget-frame-command (com-layout :name t :menu t) ()
	(with-accessors ((layout frame-current-layout)) *application-frame*
		(setf layout (if (eq layout 'default)
		                 'editmode
		                 'default))))

(defmethod clim:handle-repaint ((gadget generic-simple-gadget) region)
	(declare (ignore region))
	(loop for row from 0 to 10 do
		(loop for col from 0 to 20 do
			(clim:draw-rectangle* gadget
			                      (* 10 row)
			                      (* 10 col)
			                      (+ 10 (* 10 row))
			                      (+ 10 (* 10 col))
			                      :ink (gadget-color gadget)))))

(defmethod clim:handle-event ((gadget generic-simple-gadget) (event clim:pointer-button-press-event))
	(declare (ignore event))
	(setf (gadget-color gadget) clim:+darkblue+)
	(clim:repaint-sheet gadget clim:+everywhere+))

(defmethod clim:handle-event ((gadget generic-simple-gadget) (event clim:pointer-button-release-event))
	(declare (ignore event))
	(setf (gadget-color gadget) clim:+darkred+)
	(clim:repaint-sheet gadget clim:+everywhere+))

(defmethod clim:handle-event ((gadget generic-simple-gadget) (event clim:key-press-event))
	(format t "Keypress: name: ~s, char: ~s~%"
		(clim:keyboard-event-key-name event)
		(clim:keyboard-event-character event))
	(if (equal (clim:keyboard-event-character event) #\q)
		(setf (gadget-color gadget) clim:+darkgreen+))
	(clim:repaint-sheet gadget clim:+everywhere+))


(run 'gadget-frame)

#!/bin/sh

# Uses quicklisp to load in CLPython
sbcl --eval "(progn*
	(ql:quickload \"mcclim\")
	(sb-ext:save-lisp-and-die \"sbclclim\" :purify t :executable t))"


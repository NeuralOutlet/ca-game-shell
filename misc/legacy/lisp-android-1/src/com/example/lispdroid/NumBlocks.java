
package com.example.lispdroid;

import android.content.res.AssetManager;
import android.widget.TextView;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;

public class NumBlocks extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// Display assets
		AssetManager assetManager = getAssets();
		try{
			String[] files = assetManager.list("");
			for(String line : files)
				Log.d("JAVA", "Asset: " + line);
		}catch(IOException e) {
			e.printStackTrace();
		}

		// Initial setup of ECL
		int testCell;
		init("/data/lisp_files/");

		// Check a start value
		testCell = getCellState(6,6);
		Log.d("LISP", "Cell[6,6] = " + testCell);

		// Set it to something new
		// then check it again
		setCellState(6,6,1);
		testCell = getCellState(6,6);
		Log.d("LISP", "Cell[6,6] = " + testCell);

	}

	private static native void init(String path);
	private static native int getCellState(int x, int y);
	private static native void setCellState(int x, int y, int state);

	static {
		System.loadLibrary("ecl");
		System.loadLibrary("lispgame");
		Log.w("LISP", "Done loading the library");
	}
}

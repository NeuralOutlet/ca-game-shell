
# Android version (Using ECL & JNI) #

Embedded Common Lisp (ECL) is lovely and magical and compatible with C.
Java Native Interface (JNI) is lovely and magical and allows C/Java interaction.
Android is whimsical.

### Compiling and running from here ###

Requirements: GCC, ECL, Java, ant, Android NDK, Android SDK.

    $ $SDK_PATH/tools/android update project -t android-9 -p .
    $ ln -s /path/to/ecl/ecl-android ecl-android
    $ ln -s ecl-android/lib/ecl-16.1.2 ecl-libdir
    $ $NDK_PATH/ndk-build
    $ ant debug install

Before running:

    $ adb shell mkdir data/lisp_files
    $ adb push ../lisp-game/tetris.lisp /data/lisp_files/

Nice.

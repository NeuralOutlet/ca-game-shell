
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <ecl/ecl.h>

#include <android/log.h>
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "LISP", __VA_ARGS__))

JNIEXPORT void JNICALL
Java_com_example_lispdroid_NumBlocks_init(JNIEnv *env, jclass obj, jstring path)
{
	static int already_set = 0;
	const char *lisp_dir = (*env)->GetStringUTFChars(env, path, NULL);
	if (already_set) {
		return;
	}
	already_set = 1;

	char tmp[2048];
	LOGI("Setting the directories\n");
	snprintf(tmp, 2048, "%s/", lisp_dir);
	setenv("ROOT", tmp, 1);

	// Initialise ECL interpreter and load game
	char *ecl = "ecl";
	cl_boot(1, &ecl);

	// Set the default path, package, and load the game
	CL_CATCH_ALL_BEGIN(ecl_process_env())
	{
		sprintf(tmp, "(setq *default-pathname-defaults* #p\"%s/\")", getenv("ROOT"));
		LOGI("Set root path: %s", getenv("ROOT"));
		si_safe_eval(3, c_string_to_object(tmp), Cnil, OBJNULL);
		LOGI("Select CL-USER");
		si_select_package(ecl_make_simple_base_string("CL-USER", 7));
		LOGI("Load tetris.lisp file");
		si_safe_eval(3, c_string_to_object("(load \"tetris.lisp\")"), Cnil, OBJNULL);
	} CL_CATCH_ALL_END;

	return;
}

JNIEXPORT void JNICALL
Java_com_example_lispdroid_NumBlocks_setCellState(JNIEnv *env, jclass obj, jint x, jint y, jint state)
{
	char str[256];
	sprintf(str, "(set-cell-state %d %d %d)", x, y, state);
	cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return;
}

JNIEXPORT jint JNICALL
Java_com_example_lispdroid_NumBlocks_getCellState(JNIEnv *env, jclass obj, jint x, jint y)
{
	jint output;
	char str[256];
	sprintf(str, "(get-cell-state %d %d)", x, y);
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	output = (int)ecl_to_float(result);
	return output;
}

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Alternate_Tetris
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        List<AutoCell> cellList = new List<AutoCell>();
        UserCell user;

        Block test;

        Random random = new Random();



        string[] CellColour = { "Blue Cell", "Green Cell", "Purple Cell", "Red Cell", "Yellow Cell" };


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 500;
            graphics.PreferredBackBufferHeight = 750;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            int num = random.Next(0, 5);
            string randColour = @"Images/" + CellColour[num];
            user = new UserCell(Content.Load<Texture2D>(randColour), Vector2.Zero, 'O');

            test = new Block("Zshape", new Vector2(200f, 50f), Content.Load<Texture2D>(randColour));

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            bool end = false;
            foreach (AutoCell other in cellList)
                if (other.GetPos().X < 0)
                    end = true;

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || end)
                this.Exit();

            


            user.Update(gameTime, Window.ClientBounds);
            foreach (AutoCell other in cellList)
            {
                other.Update(gameTime, Window.ClientBounds);


                if (other.GetActive() == false)
                {
                    //-================================================-
                    user.CheckAgainst(other);
                    //-================================================-
                }

                foreach (AutoCell cell in cellList)
                    if (cell.GetPos().X == Window.ClientBounds.Width - 50)
                        CheckLink(cell.GetPos());


                if (other.GetValue() == 10)
                    other.CheckDelete(true);
            }

            foreach (AutoCell block in cellList)
                foreach (AutoCell otherBlock in cellList)
                    if (otherBlock != block)
                    {
                        if (block.GetPos().X == otherBlock.GetPos().X &&
                            block.GetPos().Y > otherBlock.GetPos().Y)
                            block.CheckDelete(otherBlock.GetDelete());

                        if (block.GetActive())
                            if (block.GetPos().Y + 50 > otherBlock.GetPos().Y
                                && block.GetPos().X == otherBlock.GetPos().X)
                            {
                                block.SetStatic(true);
                                block.UpdateValue(otherBlock.GetValue());
                            }
                            else
                            {
                                block.SetStatic(false);
                                block.UpdateValue(0);
                            }
                    }

            List<AutoCell> deathList = new List<AutoCell>();
            foreach (AutoCell other in cellList)
            {
                if (other.GetDelete() || other.GetPointsDel())
                    deathList.Add(other);
            }

            foreach (AutoCell other in deathList)
            {
                if (other.GetValue() == 1 && other.GetPointsDel() == false)
                    cellList.Add(new AutoCell(Content.Load<Texture2D>(@"Images/Grey Cell"), new Vector2(other.GetPos().X - 50, 100), 0, true));

                cellList.Remove(other);
            }

            if (user.Static())
            {
                //Add cell to the auto list where it lands:
                cellList.Add(new AutoCell(Content.Load<Texture2D>(@"Images/Grey Cell"), user.GetPos(), user.GetValue(), false));

                //Reset user block ("spawn new"):
                int posnum = random.Next(0, 10);
                int colnum = random.Next(0, 5); //colours not grey
                string randColour = @"Images/" + CellColour[colnum];
                user.Reset(Content.Load<Texture2D>(randColour), new Vector2(((float)posnum * 50f), 0f));
            }



            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SlateGray);

            user.Draw(gameTime, spriteBatch);

            foreach (AutoCell cell in cellList)
            {
                cell.Draw(gameTime, spriteBatch);
            }

            base.Draw(gameTime);
        }

        public bool CheckLink(Vector2 validPos)
        {
            foreach (AutoCell cell in cellList)
            {
                if (cell.GetPos() == validPos)
                {
                    if (cell.GetPos().X == 0f)
                    {
                        return cell.PassPointsDel(true);
                    }
                    else
                        return cell.PassPointsDel(CheckLink(new Vector2(validPos.X - 50f, validPos.Y)));
                }
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Alternate_Tetris
{
    class AutoCell: Cell
    {
        public AutoCell(Texture2D texture, Vector2 position, int value, bool active)
            : base(texture, position)
        {
            if (active)
                canMoveDown = true;
            else
                canMoveDown = false;

            this.value = value;
            gravity = 5f;
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            
            if (position.Y + texture.Height < clientBounds.Height && canMoveDown)
                position += direction;
            else
                canMoveDown = false;

            if (position.Y + texture.Height == clientBounds.Height)
                value = 1;

            base.Update(gameTime, clientBounds);
        }

        public override Vector2 direction
        {
            get 
            {
                return new Vector2(0f, gravity);
            }
        }

        public int LinkValue(List<AutoCell> cells, Vector2 checkvec)
        {
            foreach(AutoCell cell in cells)
                if(cell.GetPos() == checkvec)
                {
                    if (cell.GetPos().Y +50 == 750f)
                    {
                        cell.UpdateValue(0);
                        return 500;
                    }
                    else
                        return LinkValue(cells, (new Vector2(checkvec.X, checkvec.Y - 50f)));
                }
            return 8;
        }


        public bool GetActive()
        {
            return canMoveDown;
        }

        public void SetStatic(bool isStatic)
        {
            canMoveDown = !isStatic;
        }

        public bool PassPointsDel(bool setPointsDel)
        {
            pointsDel = setPointsDel;
            return pointsDel;
        }

        public bool GetPointsDel()
        {
            return pointsDel;
        }
    }
}

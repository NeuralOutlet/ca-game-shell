﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


/* Rotation Rules:
 * | A | B | C |
 * | H | O | D |
 * | G | F | E |
 */

namespace Alternate_Tetris
{
    class Block
    {
        protected List<UserCell> blocks = new List<UserCell>();
        protected bool canRotate = true;
        protected bool frozen = false;

        string[] types = {"line","square","Lshape","Zshape","Tshape"};

        public void Freeze()
        {
            canRotate = false;
            frozen = true;
            foreach (UserCell cell in blocks)
                cell.SetStatic(true);
        }

        public void SetMoveLeft(bool canMove)
        {
            foreach (UserCell cell in blocks)
                cell.CheckLeft(canMove);
        }

        public void SetMoveRight(bool canMove)
        {
            foreach (UserCell cell in blocks)
                cell.CheckRight(canMove);
        }


        public bool GetFrozen()
        {
            return frozen;
        }

        public Block(string type, Vector2 originPos, Texture2D texture)
        {
            SetType(type, originPos, texture);
        }
        

        private void SetType(string type, Vector2 originPos, Texture2D texture)
            {
            if (type == "line")
            {
                blocks.Add(new UserCell(texture, new Vector2(originPos.X, originPos.Y - 50f), 'B'));
                blocks.Add(new UserCell(texture, originPos, 'O'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X, originPos.Y + 50f), 'F'));
            }

            if (type == "square")
            {
                blocks.Add(new UserCell(texture, originPos, 'O'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X + 50f, originPos.Y), 'D'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X + 50f, originPos.Y + 50f), 'E'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X, originPos.Y + 50f), 'F'));
            }

            if(type == "Tshape")
            {
                blocks.Add(new UserCell(texture, new Vector2(originPos.X, originPos.Y - 50f), 'B'));
                blocks.Add(new UserCell(texture, originPos, 'O'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X + 50f, originPos.Y), 'D'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X - 50f, originPos.Y), 'H'));
            }

            if (type == "Lshape")
            {
                blocks.Add(new UserCell(texture, new Vector2(originPos.X - 50f, originPos.Y - 50f), 'A'));
                blocks.Add(new UserCell(texture, originPos, 'O'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X + 50f, originPos.Y), 'D'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X - 50f, originPos.Y), 'H'));
            }

            if (type == "Zshape")
            {
                blocks.Add(new UserCell(texture, new Vector2(originPos.X - 50f, originPos.Y - 50f), 'A'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X, originPos.Y - 50f), 'B'));
                blocks.Add(new UserCell(texture, originPos, 'O'));
                blocks.Add(new UserCell(texture, new Vector2(originPos.X + 50f, originPos.Y), 'D'));
            }
        }

        public void Update(GameTime gameTime, Rectangle clientBounds)
        {
            foreach (UserCell cell in blocks)
            {
                if (cell.GetLeftMove() == false)
                    foreach (UserCell allcells in blocks)
                        allcells.CheckLeft(false);

                if (cell.GetRightMove() == false)
                    foreach (UserCell allcells in blocks)
                        allcells.CheckRight(false);
            }

            foreach (UserCell cell in blocks)
            {
                cell.Update(gameTime, clientBounds);
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Keyboard.GetState().IsKeyUp(Keys.Space))
            {
                canRotate = true;
            }
            
            foreach (UserCell cell in blocks)
                cell.Draw(gameTime, spriteBatch);

        }

        /*public void Move(GameTime gameTime, Rectangle clientBounds, List<AutoCell> staticCells)
        {
            //Checkbounds:
            foreach (UserCell aCell in blocks)
            {
                //Bounds:
                if ((aCell.GetPos().X + 50) >= clientBounds.Width)
                    aCell.CheckRight(false); // Reached the right wall

                if (aCell.GetPos().X <= 0) //Reached the left wall
                    aCell.CheckLeft(false);

                //Cell Bounds:
                foreach (AutoCell bCell in staticCells)
                    if (bCell.GetActive() == false) //check bounds between cells
                    {
                        //-================================================-
                        if (aCell.GetPos().Y + 50 == bCell.GetPos().Y
                            &&  aCell.GetPos().X == bCell.GetPos().X)
                        {
                            aCell.SetStatic(true);
                        }
                        //-================================================-

                        if (aCell.GetPos().X == bCell.GetPos().X + 50
                            && aCell.GetPos().Y + 50 == bCell.GetPos().Y)
                        {
                            aCell.CheckLeft(false);
                        }

                        if (aCell.GetPos().X + 50 == bCell.GetPos().X
                            && aCell.GetPos().Y + 50 == bCell.GetPos().Y)
                        {
                            aCell.CheckRight(false);
                        }
                        //-===============================================-
                        
                    }

                

            }
        }*/

        public void Rotate()
        {
            if(canRotate)
            {

                foreach (UserCell cell in blocks)
                {
                    if (cell.GetID() == 'A')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X + 100f, cell.GetPos().Y));
                        canRotate = false;
                        cell.setID('C');
                    }
                    else if (cell.GetID() == 'C')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X, cell.GetPos().Y + 100f));
                        canRotate = false;
                        cell.setID('E');
                    }
                    else if (cell.GetID() == 'E')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X - 100f, cell.GetPos().Y));
                        canRotate = false;
                        cell.setID('G');
                    }
                    else if (cell.GetID() == 'G')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X, cell.GetPos().Y - 100f));
                        canRotate = false;
                        cell.setID('A');
                    }
                    else if (cell.GetID() == 'B')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X + 50f, cell.GetPos().Y + 50f));
                        canRotate = false;
                        cell.setID('D');
                    }
                    else if (cell.GetID() == 'D')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X - 50f, cell.GetPos().Y + 50f));
                        canRotate = false;
                        cell.setID('F');
                    }
                    else if (cell.GetID() == 'F')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X - 50f, cell.GetPos().Y - 50f));
                        canRotate = false;
                        cell.setID('H');
                    }
                    else if (cell.GetID() == 'H')
                    {
                        cell.Reset(new Vector2(cell.GetPos().X + 50f, cell.GetPos().Y - 50f));
                        canRotate = false;
                        cell.setID('B');
                    }
                    else { }
                }
            }
        }

        public List<UserCell> GetCells()
        {
            return blocks;
        }

        public void SetCells(List<UserCell> cells)
        {
            blocks = cells;
        }

        public void Reset(string type, Vector2 originPos, Texture2D texture)
        {
            blocks.Clear();
            SetType(type, originPos, texture);
            frozen = false;
        }

        public bool Rotatable(Rectangle clientBounds, List<AutoCell> others)
        {
            bool output = false;

            foreach (UserCell cell in blocks)
                if (cell.GetPos().X > 0 && cell.GetPos().X + 50 < clientBounds.Width)
                    output = true;
                else
                    output = false;

            foreach (UserCell aCell in blocks)
                foreach (AutoCell bCell in others)
                    if (aCell.GetPos().X > bCell.GetPos().X + 50 && aCell.GetPos().X + 50 < bCell.GetPos().X
                        && aCell.GetPos().Y + 50 >= bCell.GetPos().Y && aCell.GetPos().Y + 50 <= bCell.GetPos().Y)
                        output = true;

            return output;
        }
    }
}

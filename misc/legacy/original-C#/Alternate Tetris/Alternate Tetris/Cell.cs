﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Alternate_Tetris
{
    abstract class Cell
    {
        //Variables of the class
        protected Texture2D texture;
        protected Vector2 position;
        protected float gravity = 2f;
        protected int value = 0;

        protected bool deleteMe = false;
        protected bool pointsDel = false;

        protected bool canMoveLeft = true;
        protected bool canMoveRight = true;
        protected bool canMoveDown = true;

        //Constructors
        public Cell(Texture2D texture, Vector2 position)
        {
            this.texture = texture;
            this.position = position;
        }

        //Deconstructors
        

        public virtual void Update(GameTime gameTime, Rectangle clientBounds)
        {
            
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(texture, position, null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
            spriteBatch.End();
        }

        public abstract Vector2 direction
        {
            get;
        }

        public int GetValue()
        {
            return value;
        }

        public void UpdateValue(int otherValue)
        {
            if (otherValue > 0)
                value = otherValue + 1;
        }

        public Vector2 GetPos()
        {
            return position;
        }

        public void CheckDelete(bool delme)
        {
            deleteMe = delme;
        }

        public bool GetDelete()
        {
            return deleteMe;
        }
    }

}

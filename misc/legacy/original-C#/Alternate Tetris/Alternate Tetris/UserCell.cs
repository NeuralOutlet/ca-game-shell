﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Alternate_Tetris
{
    class UserCell: Cell
    {
        protected char identity; //used in rotation

        public UserCell(Texture2D texture, Vector2 position, char id)
            : base(texture, position)
        {
            identity = id;
        }

        public override Vector2 direction
        {
            get
            { 
                Vector2 inputDirection = Vector2.Zero;

                if(canMoveDown)
                    inputDirection.Y += gravity;

                if (canMoveLeft && Keyboard.GetState().IsKeyDown(Keys.Left))
                {
                    inputDirection.X -= texture.Width;
                    canMoveLeft = false;
                }

                if (canMoveRight && Keyboard.GetState().IsKeyDown(Keys.Right))
                {
                    inputDirection.X += texture.Width;
                    canMoveRight = false;
                }

                return inputDirection;
            }
        }

        public void CheckAgainst(AutoCell other)
        {
            if (/*touching a block*/position.Y + 50 > other.GetPos().Y
                 && /*within X bound*/ position.X == other.GetPos().X)
            {
                SetStatic(true);
                UpdateValue(other.GetValue());
            }

            if (position.X == other.GetPos().X + 50
                && position.Y + 50 > other.GetPos().Y)
            {
                CheckLeft(false);
            }

            if (position.X + 50 == other.GetPos().X
                && position.Y + 50 > other.GetPos().Y)
            {
                CheckRight(false);
            }
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            if (Keyboard.GetState().IsKeyUp(Keys.Left) && Keyboard.GetState().IsKeyUp(Keys.Right))
            { 
                canMoveRight = true; canMoveLeft = true; 
            } // Locking one movement at a time, extremely useful!

            //=========

            //Checkbounds:
            if ( (position.X + texture.Width) >= clientBounds.Width)
               canMoveRight = false; // Reached the right wall
           
            if(position.X <= 0) //Reached the left wall
                canMoveLeft = false;


            if (position.Y + texture.Height < clientBounds.Height && canMoveDown)
                position += direction;
            else
                canMoveDown = false;

            if (position.Y + texture.Height == clientBounds.Height)
                value = 1;


           base.Update(gameTime, clientBounds);
        }

        public bool Static()
        {
            return (!canMoveDown); //returns true if cell stopped moving
        }

        public void SetStatic(bool input)
        {
            canMoveDown = !input;
        }

        public void Reset(Texture2D newTexture, Vector2 newPosition)
        {
            texture = newTexture;
            position = newPosition;
            value = 0;
            canMoveDown = true;
        }

        public void Reset(Vector2 newPosition)
        {
            position = newPosition;
        }

        public void CheckLeft(bool canLeft)
        {
            canMoveLeft = canLeft;
        }

        public void CheckRight(bool canRight)
        {
            canMoveRight = canRight;
        }

        public char GetID()
        {
            return identity;
        }

        public void setID(char id)
        {
            identity = id;
        }

        public bool GetLeftMove()
        {
            return canMoveLeft;
        }

        public bool GetRightMove()
        {
            return canMoveRight;
        }
    }


}

#!/bin/sh

# Uses quicklisp to load in CLPython
sbcl --eval "(progn*
	(ql:quickload \"clpython\")
	(sb-ext:save-lisp-and-die \"sbclpython\" :purify t :executable t))"


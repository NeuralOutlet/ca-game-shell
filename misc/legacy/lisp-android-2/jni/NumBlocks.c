#include <jni.h>
#include <ecl/ecl.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "LISP", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "LISP", __VA_ARGS__))


void set_cell_state(int x, int y, int state)
{
	char str[256];
	sprintf(str, "(set-cell-state %d %d %d)", x, y, state);
	cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
}

int get_cell_state(int x, int y)
{
	char str[256];
	sprintf(str, "(get-cell-state %d %d)", x, y);
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return (int)ecl_to_float(result);
}


/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app* state)
{
	// Make sure glue isn't stripped.
	LOGI("Calling app_dummy function.");
	app_dummy();

	LOGI("Calling cl_boot function.");
	char *ecl = "ecl";
	cl_boot(1, &ecl);

	LOGI("Loading lisp file.");
	cl_safe_eval(c_string_to_object("(load \"/data/lisp_files/tetris.lisp\")"), Cnil, Cnil);
	printf("..\n");

	int mystate;
	mystate = get_cell_state(3,3);
	LOGI("Cell[3,3] = %d", mystate);
	set_cell_state(3,3,1);
	mystate = get_cell_state(3,3);
	LOGI("Cell[3,3] = %d", mystate);
}


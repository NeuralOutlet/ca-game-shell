
# Java version (Using ECL & JNI) #

Embedded Common Lisp (ECL) is lovely and magical and compatible with C.
Java Native Interface (JNI) is lovely and magical and allows C/Java interaction.

### Compiling and running from here ###

Requirements: GCC, ECL, Java

You will have to change make.sh to point to the correct path for jni.h

    $ chmod +x build.sh
    $ ./build.sh

Nice.

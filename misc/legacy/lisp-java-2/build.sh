#!/bin/sh

# Linux 2.6.32-358.el6.x86_64
# gcc 4.4.7
# openjdk 1.7.0

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
javac NumBlocks.java
javah NumBlocks
gcc -shared -fPIC -I/usr/lib/jvm/java-7-openjdk-amd64/include libNumBlocks.c -o libNumBlocks.so -lecl
java NumBlocks

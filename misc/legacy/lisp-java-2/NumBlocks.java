


class NumBlocks
{
	private static native void init();
	private static native int getCellState(int x, int y);
	private static native void setCellState(int x, int y, int state);

	public static void main(String[] args)
	{
		// Initial setup of ECL
		int testCell;
		init();

		// Check a start value
		testCell = getCellState(6,6);
		System.out.print("Cell[6,6] = ");
		System.out.println(testCell);

		// Set it to something new
		// then check it again
		setCellState(6,6,1);
		testCell = getCellState(6,6);
		System.out.print("Cell[6,6] = ");
		System.out.println(testCell);
	}
	static {
	System.loadLibrary("NumBlocks");
	}
}

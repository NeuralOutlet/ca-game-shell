
#include <stdio.h>
#include <ecl/ecl.h>
#include "NumBlocks.h"


JNIEXPORT void JNICALL
Java_NumBlocks_init(JNIEnv *env, jclass obj)
{
	// Initialise ECL interpreter and load game
	char *ecl = "ecl";
	cl_boot(1, &ecl);

	// Load in game code
	cl_safe_eval(c_string_to_object("(load \"../ca_shell.lisp\")"), Cnil, Cnil);
	printf("..\n");
	return;
}

JNIEXPORT void JNICALL
Java_NumBlocks_setCellState(JNIEnv *env, jclass obj, jint x, jint y, jint state)
{
	char str[256];
	sprintf(str, "(set-cell-state *board* %d %d (complex %d))", (int)x, (int)y, (int)state);
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	return;
}

JNIEXPORT jint JNICALL
Java_NumBlocks_getCellState(JNIEnv *env, jclass obj, jint x, jint y)
{
	jint output;
	char str[256];
	sprintf(str, "(get-cell-real-state %d %d)", x, y);
	cl_object result = cl_safe_eval(c_string_to_object(str), Cnil, Cnil);
	output = (int)ecl_to_float(result);
	return output;
}



;;; === Start Quicklisp and load CFFI ===
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
	(when (probe-file quicklisp-init)
			(load quicklisp-init)))
(ql:quickload "cffi")
;;; =====================================

(defpackage :cffi-user
    (:use :common-lisp :cffi))

(in-package :cffi-user)

;;; ---------------------------- ;;;
;;; The following Code is in Dev ;;;
;;; ---------------------------- ;;;

;; Easy conversion from token
(defun get-ctype (type)
	(cond
		((equal "int" type) :int)
		((equal "char" type) :char)
		(t :void)))

;; String append function for "thi" += 's'
(defun strapp (str chr)
	(concatenate 'string str (string chr)))

;; Classic whitespace
(defun isspace (chr)
	(numberp (position chr '(#\Newline #\Space #\Tab #\Return #\Linefeed))))

;; C specific token delimeters
(defun isdelim (chr)
	(numberp (position chr '(#\( #\) #\; #\, #\{ #\}))))

;; Parses the tokens for parameters
;; (int foo , int bar)         ; the form of a C function
;; into ((int foo) (int bar))  ; the form of (defcfun ...)
(defun cparse-params (params)
	(let ((lst '()))
		(loop for i from 0 below (length params)
			when (not (equal (nth i params) ","))
				do (setq lst (append lst (list (nth i params))))
			when (or (equal (nth i params) ",") (equal i (1- (length params))))
				collect (let ((tmplst lst))
					(setq lst '())
					tmplst))))

;; Generate the lisp code for the
;; defcfun call then evaluate it
(defun gen-cfunc (rtype name params)
	(eval (append `(defcfun ,name ,rtype)
		(loop for pair in (cparse-params params)
			collect (list (make-symbol (cadr pair))
			              (get-ctype (car pair)))))))

;; The Main function
;; TODO: Lots more
;;    multiline comments and params
;;    struct data types
;;    other basic types
;;    strings
;;    anonymous parameter declarations
(defun transpile (line)
	(if (> (length line) 1)
		(let* ((tokens (ctokens line))
		       (ast (cparse tokens)))
			(if (or (null ast) (equal (car ast) "//"))
				"empty line"
				(gen-cfunc (get-ctype (car ast)) (cadr ast) (caddr ast))))))

;; Create AST of sorts
;(of-type <type>
;	(of-func <function name>
;		(of-params <list of parameters>))
;	(of-var <variable name>
;		(of-value <value>)))
(defun cparse (tokens)
	(let ((ast (list (nth 0 tokens) (nth 1 tokens))))
		(append ast (list (loop for i from 3 below (- (length tokens) 2)
			collect (nth i tokens))))))

;; Simple tokeniser
(defun ctokens (line)
	(let ((state 0) (token "") (tokens '()))
		(setf tokens (loop for c across line
			when (isdelim c)
				append (let ((toka token) (tokb (string c)))
					(setf token "")
					(setf state 0)
					(list toka tokb))
			when (and (eq state 1) (isspace c))
				collect (let ((tok token))
					(setf token "")
					(setf state 0)
					tok)
			when (and (eq state 0) (and (not (isdelim c)) (not (isspace c))))
				do (setf state 1)
			when (and (eq state 1) (not (isspace c)))
				do (setf token (strapp token c))))
		(if (and (equal (char (car tokens) 0) #\\) (equal (char (car tokens) 1) #\\))
			""
			(remove "" tokens :test #'equal))))

;; Force PLUS to be a macro character
(make-dispatch-macro-character #\+)

;; Set the dispatch ++ to call this
;; anonymous function on the stream
;; Function: Reads in each line until
;;           '++' is reached again.
(set-dispatch-macro-character #\+ #\+
	#'(lambda (stream c n)
		(loop for line = (read-line stream nil "++" t)
			until (equal line "++")
			do (transpile line))))


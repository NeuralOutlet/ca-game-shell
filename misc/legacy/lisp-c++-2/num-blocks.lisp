
(load "../tetris.lisp")
(load "c-syntax.lisp")
 ++
 // Define c functions to be called from lisp
 // (found in libnumblocks.so)
 void draw_board(int board[][]);
 char get_user_input();
 ++

(defun get-user-input ()
	(get-user-input))

(defun set-board (board)
	(draw-board board))


;; Run the game
(game-loop *board*)

#!/bin/bash

# A build script to call all front-ends and put the compiled output into a bin folder
# Also takes a make target name for individual builds

for project in `ls interfaces`
do
	REQUIRED=""
	basedir=`pwd`

	cd interfaces/${project}
	linecount=$((`expr length ${project}` + 9))
	if [ -e ./build.sh ]; then
		printf "\033[0;35m%${linecount}s\n\033[0m" |tr " " "="
		printf "\033[0;35m%s\n\033[0m" "Project: ${project}"
		printf "\033[0;35m%${linecount}s\n\033[0m" |tr " " "="
		if [ -e ./required ]; then
			REQUIRED=`cat required`
			printf "\n\033[0;35m%s\n\033[0m" "Requirements"
		printf "\033[0;35m%${linecount}s\n\033[0m" |tr " " "-"
		fi

		buildme=true
		for thing in ${REQUIRED}
		do
			export version=`which ${thing}`
			if [ -z ${version} ]; then
				printf " \033[0;31m%-20s \t%s\033[0m\n" ${thing} '[NOT FOUND]'
				buildme=false
			else
				printf " \033[0;35m%-20s \t%s\033[0m\n" ${thing} "[${version}]"
			fi
		done

		if ${buildme}; then
			printf "\n\033[0;35m%s\n\033[0m" "Building ${project}"
			printf "\033[0;35m%${linecount}s\n\033[0m" |tr " " "-"
			./build.sh
		else
			printf "\n\033[0;31mNot building ${project}.\033[0m\n"
		fi
	
		printf "\n\n"
	fi
	cd $basedir
done
